# Puma Scraper

This Scrapy spider is designed to scrape product information from the Puma UK website (https://uk.puma.com). It can collect data about Puma products, including their brand name, title, original price, current price, descriptions, unique identifier, color options, size options, and image URLs. This README provides instructions on how to set up and run the spider to collect data from the website.

## Prerequisites

Before using this Scrapy spider, ensure you have the following prerequisites installed on your system:

- [Python](https://www.python.org/) (Python 3 recommended)
- [Scrapy](https://scrapy.org/) (You can install it using `pip install scrapy`)

## Usage

1. Clone or download this repository to your local machine:

   ```shell
   git clone https://gitlab.com/almas33/pumacrawler.git
   git checkout dev   

   
## Navigate to the project directory:
      cd pumaScraping


## Run the spider using the following command:
    scrapy crawl pumascraper -o pumascraper_data.json

# Output
The spider will create a json file named pumascraper_data.json in the project directory. This CSV file will contain the collected product data, including brand name, title, original price, current price, descriptions, unique identifier, color options, size options, and image URLs.
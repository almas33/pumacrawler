import scrapy
from scrapy.crawler import CrawlerProcess


class PumaScraperSpider(scrapy.Spider):

    name = "pumascraper"
    allowed_domains = ["uk.puma.com"]

    constant_category = {
        "Women": ["shoes", "clothing", "accessories", "sports"],
        "Men": ["shoes", "clothing", "accessories", "sports"],
        "Kids": ["footwear", "clothing", "boys/clothing", "girls/clothing", "sports", "shop-by-age"],
        "Collections": ["select", "lifestyle"]
    }
    main_url = "https://uk.puma.com"
    start_urls = [main_url]

    def parse(self, response, **kwargs):
        main_categories = response.css("li[data-test-id='main-nav-category-item']")
        all_categories_urls = []
        for main_category in main_categories:
            main_category_name = main_category.css("span::text").get()
            sub_categories = self.constant_category.get(main_category_name,'')
            if sub_categories:
                for sub_category in sub_categories:
                    main_category_url = main_category.css("a[href]::attr(href)").get()
                    final_url = self.main_url + main_category_url + "/" + sub_category
                    all_categories_urls.append(final_url)
            else:
                main_category_url = main_category.css("a[href]::attr(href)").get()
                final_url = self.main_url + main_category_url
                all_categories_urls.append(final_url)

        for category_link in all_categories_urls:
            yield scrapy.Request(category_link, callback=self.get_items_totals)

    def get_items_totals(self, response, **kwargs):
        total_item = response.css("span[data-test-id='product-results'] ::text").get().split(" ")[0]
        all_items_url = response.url + "?offset=" + total_item

        yield scrapy.Request(all_items_url, callback=self.get_items_urls)

    def get_items_urls(self, response, **kwargs):
        all_items = response.css("li[data-test-id='product-list-item']")
        for item in all_items:
            item_link = item.css("a[href]::attr(href)").get()
            full_item_link = self.main_url + item_link
            yield scrapy.Request(full_item_link, callback=self.get_items_details)

    def get_items_details(self, response, **kwargs):
        brand_name = "puma"
        title = response.css('h1#pdp-product-title::text').extract_first()
        original_price = response.css("span[data-test-id='item-price-pdp']::text").extract_first()
        current_price = response.css("span[data-test-id='item-sale-price-pdp']::text").extract_first()
        # price = response.css("span[data-test-id='item-price-pdp']::text").extract_first()
        descriptions = response.css("div[data-test-id='pdp-product-description'] .text-base div::text").extract_first()
        unique_identifier =  response.css("span.text-puma-black::text")[2].get()
        color_list = []
        for color in response.css("#style-picker .sr-only::text"):
            color_list.append(color.get())
        size_list = []
        for size in response.css("#size-picker label .text-sm::text"):
            size_list.append(size.get())

        image_list = []
        for image in response.css("section[data-test-id='product-image-gallery-section'] img[src]::attr(src)"):
            image_list.append(image.get())
        category = response.url.split('category=')[1]
        yield {
            "brand_name":brand_name, "title":title,
            "original_price":original_price, "category":category,
            "current_price":current_price, "descriptions":descriptions,
            "unique_identifier":unique_identifier, "color_list":color_list,
            "size_list":size_list, "image_list":image_list
        }


# process = CrawlerProcess({
#     'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
# })
# process.crawl(PumaScraperSpider)
# process.start()

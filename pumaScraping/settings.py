# Scrapy settings for all_pumasscraping project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = "pumaScraping"

SPIDER_MODULES = ["pumaScraping.spiders"]
NEWSPIDER_MODULE = "pumaScraping.spiders"
# DOWNLOADER_MIDDLEWARES = {
#     'scrapy_splash.SplashCookiesMiddleware': 723,
#     'scrapy_splash.SplashMiddleware': 725,
#     'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
# }
# SPIDER_MIDDLEWARES = {
#     'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
# }

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = "all_pumasscraping (+http://www.yourdomain.com)"

# Obey robots.txt rules
ROBOTSTXT_OBEY = True


DOWNLOAD_DELAY = 2
CONCURRENT_REQUESTS = 10


FEED_FORMAT = 'csv'  # Specify the output format as CSV
FEED_URI = 'output.csv'  # Define the CSV file name and path